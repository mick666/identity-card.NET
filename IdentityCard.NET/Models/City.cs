﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Identity_Card.Models
{
    public class City
    {
        public string zh_cn { get; set; }
        public string en { get; set; }
    }
}
