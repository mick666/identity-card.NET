﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Identity_Card.Models
{
    public class Gender
    {
        public string female { get; set; }
        public string male { get; set; }
    }
}
