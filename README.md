
一个简单的身份证号码获取用户信息工具 .NET版
------------------------

PHP版本 : https://gitee.com/git-ofcold-rep/identity-card
------------------------
<br>

>  中国（大陆地区）公民身份证，数据来源于国家标准GB/T 2260-2007 <a href="http://www.mca.gov.cn/article/sj/xzqh/2018/201804-12/201804-06041553.html" target="_blank">（中华人民共和国行政区划代码)</a>


## 说明
一个基于中华人民共和国公民身份证的组件可以获取用户信息。这个适用于任何.NET框架，.NET FrameWork 4.5

## 使用

#### 运行测试文件
```bash
    引用 IdentityCard.NET.dll 或 引用 IdentityCard.NET 项目
```


```C#
IdentityCard id = IdentityCard.Init("320106198310290811", "zh_cn");

string area = id.GetArea();
string gender = id.GetGender();
string birthday = id.GetBirthday();
int age = id.GetAge();
string age = id.GetConstellation();

```


#### 返回结果:
```json
{
    "area":"江苏省 南京市 鼓楼区",
    "province":"江苏省",
    "city":"南京市",
    "county":"鼓楼区",
    "Gender":"男",
    "birthday":"1983-10-29",
    "zodiac":"猪",
    "age":"34",
    "constellation":"天蝎座"
}

```

## Api
- GetArea():string `获取地区`
- GetConstellation():string `获取星座`
- GetZodiac() : string `获取生肖`
- GetAge():int `获取年龄`
- GetBirthday():string `获取生日`
- GetGender():string `获取性别`
- GetCounty():string|null `获取县城`
- GetCity():string|null `获取城市`
- GetProvince():string|null `获取省`
- ToJson():string `全部信息`